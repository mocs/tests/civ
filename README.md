# Container Image Verifier

A test suite to automatically verify the content of the produced container images so that they do not contain anything dangerous.